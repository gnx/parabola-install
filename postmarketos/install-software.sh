#!/usr/bin/env ash

echo ""
echo "==========================="
echo "| Инсталиране на програми |"
echo "==========================="
# следва масив с програми
programs="
  zsh
  zsh-completions
  zsh-autosuggestions
  zsh-syntax-highlighting
  starship
  neofetch
  neovim
  exa
  bat
"
doas apk add $programs
doas chsh -s /bin/zsh user


echo ""
echo "=========================="
echo "| Задаване на променливи |"
echo "=========================="
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export DOTFILES="$HOME/gnu-linux-installs/dotfiles"


echo ""
echo "======================================"
echo "| Създаване на необходими директории |"
echo "======================================"
mkdir -p $ZDOTDIR


echo ""
echo "================================"
echo "| Създаване на символни връзки |"
echo "================================"
ln -sf $DOTFILES/zsh/postmarketos/.zshenv $HOME/.zshenv
ln -sf $DOTFILES/zsh/postmarketos/.zshrc $XDG_CONFIG_HOME/zsh/.zshrc
ln -sf $DOTFILES/neofetch $XDG_CONFIG_HOME/neofetch
ln -sf $DOTFILES/starship/starship.toml $XDG_CONFIG_HOME/starship.toml
ln -sf $DOTFILES/nvim $XDG_CONFIG_HOME/nvim

- добавям iomem=relaxed като grub параметър
- sudo flashrom -p internal -r ~/bios-new -c MX25L6405
	- ~/bios-new е името на файла, в който ще си запиша backup-а
	- MX25L6405 е името на BIOS чипа
- изтеглям си от сайта на libreboot архив и го разархивирам
- влизам в папката (разархивираната)
- cd libreboot_r20160902_util/cbfstool/x86_64 (поставих си го в ~/.bin папката под името libreboot-cbfstool, за да няма нужда да го тегля повече от сайта на libreboot)
- libreboot-cbfstool ~/bios-new print -> трябва да съдържа много файлове и измежду тях grub.cfg
- libreboot-cbfstool ~/bios-new extract -n grubtest.cfg -f ~/grubtest.cfg -> с тази команда си извличам grubtest.cfg и го записвам в домашната си папка
- libreboot-cbfstool ~/bios-new remove -n grubtest.cfg -> изтриваме "фалът" grubtest.cfg от тази "файлова система"
- libreboot-cbfstool ~/bios-new add -n grubtest.cfg -f ~/grubtest.cfg -t raw -> добавям файлът grubtest.cfg под името grubtest.cfg
- sudo flashrom -p internal -w ~/bios-new -c MX25L6405 -> флашвам го

- когато съм си настроил BIOS-а и работи в grubtest.cfg, преминавам към финалното флашване (а то е същото като предишното, но действам върху grub.cfg, вместо върху grubtest.cfg):
  - libreboot-cbfstool ~/bios-new remove -n grub.cfg
  - libreboot-cbfstool ~/bios-new add -n grub.cfg -f ~/grubtest.cfg -t raw


#!/usr/bin/env bash

wal -R # възстанови последната ползвана тема, докато се генерира новата, защото това отнема няколко секунди, в които иначе темата на polybar и терминалите е черно-бяла, което е доста грозно

dir=/usr/share/wallpapers/hristo
wallpapers=($dir/*)

if (( $# == 0 )) # ако този скрипт е извикан без да са му подадени променливи (значи искам произволен тапет и съответната му тема)
then
  RANDOM=$(date +%s%N | cut -b10-19) # семенце за генерирането на произволни числа (секундите и наносекундите са основата му)
  wallpapers_number=$(ls -1q $dir | wc -l) # преброй колко файла има в папката с тапети
  file_num=$(( $RANDOM % wallpapers_number )) # вземи произволно число от 0 (включително) до броя на тапетите - 1, чрез остатък при целочислено деление
  file=${wallpapers[$file_num]} # вземи името на файлът с този номер
else # ако е извикан с параметър (значи искам да му задам конктерен тапет)
  file=${wallpapers[$1]} # вземи файлът с номерът, който е подаден при извикването на този скрипт
fi

current=/home/$(whoami)/.local/share/wallpapers/current-wallpaper # прекият път към файла с настоящия тапет; няма разширение, защото може да е .jpg, .png, .svg и всички други
ln -sf $file $current # -s означава symbolyc link, а -f замества файла, ако вече съществува, вместо да върне грешка
#hsetroot -cover $current # задай тапета без замъгляване, защото замъгляването отнема няколко секунди, през които за тапет стои сив цвят
hsetroot -cover $current -blur 100 # задай тапета на работния плот със замъгляването
rm -v -r ~/.cache/thumbnails/neofetch # изтрий запазените изображения в neofetch, защото като види старото няма да генерира ново
rm -v -r ~/.cache/wal # изтрий и старата тема на pywal поради същата причина
wal -n -i "$file" # задай палитра с pywal, без да задаваш тапет на работния плот, защото вече го направихме по-горе

# промени темата на leftwm
sed -i "s/default_border_color.*$/default_border_color = \"$(xrdb -get color0)\"/" /home/$(whoami)/.config/leftwm/themes/current/theme.toml
sed -i "s/floating_border_color.*$/floating_border_color = \"$(xrdb -get color8)\"/" /home/$(whoami)/.config/leftwm/themes/current/theme.toml
sed -i "s/focused_border_color.*$/focused_border_color = \"$(xrdb -get color5)\"/" /home/$(whoami)/.config/leftwm/themes/current/theme.toml
leftwm command SoftReload

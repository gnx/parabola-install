#!/usr/bin/env python

# Скрипт за презареждане на цветовете от pywal във всички отворени вече терминали
# Причината да го напиша е, че xplr не приема темата на терминала от който е извикан,
# ако не е бил xterm (не работи при st например)

import pywal

def main():
    # прочети цветовете, които pywal вече е създал
    colors = pywal.colors.file("/home/hristo/.cache/wal/colors.json")

    # приложи темата върху всички вече отворени терминали
    pywal.sequences.send(colors)
main()

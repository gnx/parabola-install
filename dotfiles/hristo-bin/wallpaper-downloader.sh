#!/usr/bin/env bash

urls=(
  https://upload.wikimedia.org/wikipedia/commons/1/1b/15-033i1-JupiterMoon-Ganymede-Aurora-20150312.jpg
  https://upload.wikimedia.org/wikipedia/commons/9/92/Atami_city_night-View.jpg
  https://upload.wikimedia.org/wikipedia/commons/0/04/Central_Pier_9.jpg
  https://upload.wikimedia.org/wikipedia/commons/8/89/Hubble_Sees_Cosmic_Flapping_%E2%80%98Bat_Shadow%E2%80%99_-_Flickr_-_NASA_Goddard_Photo_and_Video.png
  https://upload.wikimedia.org/wikipedia/commons/c/c4/Night_City_-_14024605887.jpg
  https://upload.wikimedia.org/wikipedia/commons/2/23/UB_City_at_night_.jpg
  https://upload.wikimedia.org/wikipedia/commons/5/5c/White-fronted_bee-eater_%28Merops_bullockoides%29_Namibia.jpg
)

mkdir -p /usr/share/wallpapers/hristo
for i in "${urls[@]}"
do
  wget --directory-prefix="/usr/share/wallpapers/hristo" $i
done

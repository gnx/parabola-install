# № 3 - бива прочетен след .zprofile и е само за интерактивни обвивки

# режим
bindkey -e # пусни zsh в emacs режим, а не във vi режим

# история
HISTFILE=$ZDOTDIR/.zshhistory # име на файлът, в който ще се записва историята
HISTSIZE=1000 # колко реда да прочете от HISTFILE в началото на интерактивната сесия
SAVEHIST=1000 # колко реда да запише там в края ѝ
setopt INC_APPEND_HISTORY # добавяй новата история към файла, не го изтривай всеки път
setopt SHARE_HISTORY # различните инстанции на zsh да проверяват HISTORY файла за промени от други инстанции
setopt EXTENDED_HISTORY # записвай не само командата, а и времето и продължителността ѝ
setopt HIST_IGNORE_ALL_DUPS # ако една команда се повтаря, запази само най-новото ѝ срещане
setopt HIST_REDUCE_BLANKS # премахни излишните интервали от командите (може да се окажат повече на брой еднакви команди така)
setopt NO_HIST_BEEP # премахни писукатено, когато сме достигнали края на файла с историята

# писукане
setopt NO_BEEP # премахни писуканията, когато нещо не се харесва на zsh

# дописване на командите
autoload -U compinit
compinit
_comp_options+=(globdots) # дописването работи и за скрити файлове
# setopt MENU_COMPLETE # автоматично маркирай първия елемент от списъка
zstyle ':completion:*' menu select # позволява ми да навигирам из списъка с предложения
zstyle ':completion:*' completer _extensions _complete _approximate # опитва се да ми поправи грешките
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (грешки: %e) -!%f' # показва ги цветно
zstyle ':completion:*:warnings' format ' %F{red}-- няма съвпадения --%f' # както и ако не може да ги поправи
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f' # или ако просто има да ми изпише нещо друго
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' # ако не откриеш нещо близко до написаното, опитай без да отчиташ малки и главни букви
zstyle ':completion:*' file-sort change # когато ми предлагаш дописване на файл, постави първо онези, които съм променял най-скоро


# Зареди палитрата от 'pywal' асинхронно
(cat ~/.cache/wal/sequences &) # & изпълнява процеса във фонов режим, ( ) скрива съобщенията, които инача биха се принтирали
source ~/.cache/wal/colors.sh # вкарай цветовете като променливи, за да мога да ги ползвам в скриптове
source ~/.cache/wal/colors-tty.sh # направи го и за tty


# зареждане на приставки (които преди това трябва да съм инсталирал на компютъра)
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh # оцветяване, има го пакетирано в pacman
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh # дописване на предложенията в по-блед цвят

# изведи информация за системата ми
neofetch

# подсказка (това е преводът ми на думата prompt)
eval "$(starship init zsh)"

# съкратени команди
alias ke="khard edit"
alias kl="khard list --fields index,name,emails.internet.0"


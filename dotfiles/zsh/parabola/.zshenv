# № 1 - стартира се първо

# XDG папки
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# мои променливи, които ползвам по време на инсталацията и докато си тествам parabola-hristo скриптовете и които заместват $HOME и $XDG променливите на средата, за да останат оригиналните незасегнати за останалите скриптове, които изпълнявам по време на инсталацията, но не съм си писал аз, а ги изпълнявам от root потребителя, не от профила създаден при инсталирането
export USER_HOME="/home/$(whoami)"
export DOTFILES="$USER_HOME/gnu-linux-installs/dotfiles"
export CONFIG="$USER_HOME/.config"
export LOCAL="$USER_HOME/.local"

# добавяне на .bin към PATH
export PATH=$PATH:~/.local/bin:~/.local/bin/hristo

# zsh папка с настройки
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

# за да се отварят успешно java програми в leftwm
export _JAVA_AWT_WM_NONREPARENTING=1

# програми по подразбиране
export TERM=st # терминалът
export EDITOR=nvim # задавам текстовия редактор по подразбиране за системата
export VISUAL=nvim # както и за специализиран редактор
export PAGER=less # paru ползва тази променлива, за да показва инсталационните скриптове

# освобождаване на HOME папката от ненужни файлове
export LESSHISTFILE="$XDG_CACHE_HOME"/lesshst

# задай на терминалните програми, че терминалът поддържа 24 битови цветове
export COLORTERM=truecolor

# оцвети man pages
export LESS_TERMCAP_md=$(tput bold; tput setaf 1)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
export LESS_TERMCAP_us=$(tput bold; tput setaf 2)
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4)
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)

# кръпки
export LIBGL_ALWAYS_SOFTWARE=1 # за да се стартира успешно alacritty и naikari
export QT_QPA_PLATFORMTHEME=qt5ct # за да се стартират qt приложенията през темата от qt5ct, който взема темата от kvantum и ѝ добавя шрифтове

export PARABOLA_DOTFILES="$HOME/parabola-hristo/dotfiles"

# mates - контакти
export MATES_DIR="$HOME/.contacts/posteo/default" # папката с контакти, където ще ги търси mates
export MATES_INDEX="$XDG_CACHE_HOME/mates_index" # и тази, в която да си създаде индекса

# notmuch - индексиране на писмата
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME/notmuch/config"

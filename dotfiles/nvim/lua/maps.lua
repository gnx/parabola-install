function nmap(from, to, descr)
  for k,v in ipairs(from) do
    vim.keymap.set('n', v, to, {desc = descr}) -- промени го за Normal режима
    vim.keymap.set('o', v, to, {desc = descr}) -- както и за Operator-pending режима
    vim.keymap.set('v', v, to, {desc = descr}) -- както и за Visual режима
  end
end

nmap({'а', 'a'}, '', '')
nmap({'б', 'b'}, '', '')
nmap({'в', 'w'}, 'o', 'вмъкни ред след този')
nmap({'В', 'W'}, 'O', 'вмъкни ред преди този')
nmap({'г', 'g'}, 'v', 'гледай това')
nmap({'Г', 'G'}, '', '')
nmap({'д', 'd'}, 'w', 'дума напред (началото на следващата)')
nmap({'Д', 'D'}, 'b', 'дума назад (началото на тази или на предишната)')
nmap({'е', 'e'}, '', '')
nmap({'ж', 'v'}, '', '')
nmap({'з', 'z'}, 'y', 'запомни')
nmap({'и', 'i'}, 'k', 'горе')
nmap({'й', 'j'}, 'h', 'ляво')
nmap({'к', 'k'}, 'j', 'долу')
nmap({'л', 'l'}, 'l', 'дясно')
nmap({'м', 'm'}, 'u', 'минало')
nmap({'М', 'M'}, '<C-r>', 'бъдеще')
nmap({'н', 'n'}, 'p', 'напиши след')
nmap({'Н', 'N'}, 'P', 'напиши преди')
nmap({'о', 'o'}, 'g', 'отиди на знак')
nmap({'О', 'O'}, 'G', 'отиди на ред')
nmap({'п', 'p'}, 'a', 'пиши след')
nmap({'П', 'P'}, 'i', 'пиши преди')
nmap({'р', 'r'}, 'd', 'режи')
nmap({'с', 's'}, 'r', 'смени')
nmap({'т', 't'}, 'x', 'трий')
nmap({'у', 'u'}, '', '')
nmap({'ф', 'f'}, '', '')
nmap({'х', 'h'}, '', '')
nmap({'ц', 'c'}, '', '')
nmap({'ч', '`'}, '', '')
nmap({'ш', '['}, '', '')
nmap({'щ', ']'}, '', '')
nmap({'ъ', 'y'}, 'n', 'следващ (при търсене)')
nmap({'Ъ', 'Y'}, 'N', 'предишен (при търсене)')
nmap({'ь', 'x'}, '', '')
nmap({'ю', '\\'}, '', '')
nmap({'я', 'q'}, '', '')

function ovmap(from, to, descr)
  for k,v in ipairs(from) do
    vim.keymap.set('o', v, to, {desc = descr}) -- както и за Operator-pending режима
    vim.keymap.set('v', v, to, {desc = descr}) -- както и за Visual режима
  end
end

ovmap({'д', 'd'}, 'aw', 'думата върху която е курсорът')
ovmap({'и', 'i'}, 'as', 'изречението върху което е курсорът')
ovmap({'а', 'a'}, 'ap', 'абзаца върху който е курсорът')

function cmap(from, to, descr)
  vim.keymap.set('c', from, to, {desc = descr}) -- промени го за Command-line режима
end

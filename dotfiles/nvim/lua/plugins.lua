return require('packer').startup(function()
  
  -- packer ще обновява сам себе си
  use 'wbthomason/packer.nvim' 

  -- pywal добавка
  use { 'AlphaTechnolog/pywal.nvim', as = 'pywal' }

  -- лента на състоянието
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true },
  }

  -- оцветяване според програмния език, но по-развито от основното в nvim,
  -- защото прочита кода и търси и имена на променливи и функции
  use {
    'nvim-treesitter/nvim-treesitter'
  }
  
  -- прозрачен фон
  use {
    'xiyaowong/nvim-transparent'
  }
end)

#!/usr/bin/env bash

#sudo apt update
#sudo apt -y upgrade

## Zavisimosti
# Obshti
#sudo apt -y install curl sudo unzip vim

# NodeJS
# instrukcii: https://github.com/nodesource/distributions
#sudo apt -y install ca-certificates gnupg
#sudo mkdir -p /etc/apt/keyrings
#curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
#NODE_MAJOR=20
#echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
#sudo apt-get update
#sudo apt-get -y install nodejs

# yarn
#sudo npm install --global yarn

# python
#sudo apt -y install python3-dev python-is-python3

# drugi
#sudo apt -y install certbot nginx ffmpeg postgresql postgresql-contrib openssl g++ make redis-server git cron wget

## Potrebitel
#sudo useradd -m -d /var/www/peertube -s /bin/bash -p peertube peertube
#sudo passwd peertube

## Baza danni
#cd /var/www/peertube
#sudo -u postgres createuser -P peertube
#sudo -u postgres createdb -O peertube -E UTF8 -T template0 peertube_prod
#sudo -u postgres psql -c "CREATE EXTENSION pg_trgm;" peertube_prod
#sudo -u postgres psql -c "CREATE EXTENSION unaccent;" peertube_prod

## Podgotovka na direktoriqta
#VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) && echo "Latest Peertube version is $VERSION"
#cd /var/www/peertube
#sudo -u peertube mkdir config storage versions
#sudo -u peertube chmod 750 config/
#cd /var/www/peertube/versions

## Instalirai PeerTube
#sudo -u peertube wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip"
#sudo -u peertube unzip -q peertube-${VERSION}.zip && sudo -u peertube rm peertube-${VERSION}.zip
#cd /var/www/peertube
#sudo -u peertube ln -s versions/peertube-${VERSION} ./peertube-latest
#cd ./peertube-latest && sudo -H -u peertube yarn install --production --pure-lockfile

## Konfiguraciq
#cd /var/www/peertube
#sudo -u peertube cp peertube-latest/config/default.yaml config/default.yaml
#cd /var/www/peertube
#sudo -u peertube cp peertube-latest/config/production.yaml.example config/production.yaml

## Webserver config
sudo cp /var/www/peertube/peertube-latest/support/nginx/peertube /etc/nginx/sites-available/peertube
sudo sed -i 's/${WEBSERVER_HOST}/[hristo-nqma-domein]/g' /etc/nginx/sites-available/peertube
sudo sed -i 's/${PEERTUBE_HOST}/127.0.0.1:9000/g' /etc/nginx/sites-available/peertube
#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "=============================================================================="
echo "| Задай на скрипта да се прекрати, ако някоя команда върне код различен от 0 |"
echo "=============================================================================="
set -e

echo ""
echo "$filename"
echo "============================"
echo "| Задаване на променливите |"
echo "============================"
if (( $# == 0 )) # ако този скрипт е извикан без да са му подадени променливи (значи го изпълнявам самостоятелно)
then
  read -p "Disk: " diskname # -p извежда надписа след себе си като подсказка и не преминава на нов ред
  read -p "Компютър: " compname
  read -p "Потребител: " username
  read -s -p "Парола: " password # -s е за да не се извежда паролата на екрана докато я пишем
else # ако е извикан с променливи (значи го вика главния скрипт и вече съм ги задал в него)
  diskname="$1"
  compname="$2"
  username="$3"
  password="$4"
fi

echo ""
echo "$filename"
echo "===================================="
echo "| Копирай и изпълни chroot скрипта |"
echo "===================================="
cp 3-1-tty-setup-chroot.sh /mnt/3-1-tty-setup-chroot.sh
sed -i "s|^wifiname=|wifiname=${wifiname}|" /mnt/3-1-tty-setup-chroot.sh
sed -i "s|^wifipass=|wifipass=${wifipass}|" /mnt/3-1-tty-setup-chroot.sh
sed -i "s|^diskname=|diskname=${diskname}|" /mnt/3-1-tty-setup-chroot.sh
sed -i "s|^compname=|compname=${compname}|" /mnt/3-1-tty-setup-chroot.sh
sed -i "s|^username=|username=${username}|" /mnt/3-1-tty-setup-chroot.sh
sed -i "s|^password=|password=${password}|" /mnt/3-1-tty-setup-chroot.sh
chmod +x /mnt/3-1-tty-setup-chroot.sh
arch-chroot /mnt ./3-1-tty-setup-chroot.sh
rm /mnt/3-1-tty-setup-chroot.sh

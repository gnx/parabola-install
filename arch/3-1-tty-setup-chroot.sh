#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "=============================================================================="
echo "| Задай на скрипта да се прекрати, ако някоя команда върне код различен от 0 |"
echo "=============================================================================="
set -e

echo ""
echo "$filename"
echo "============================"
echo "| Задаване на променливите |"
echo "============================"
wifiname=
wifipass=
diskname=
compname=
username=
password=

echo ""
echo "$filename"
echo "==============="
echo "| Локализация |"
echo "==============="
echo LANG=\"bg_BG.UTF-8\" > /etc/locale.conf
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
sed -i 's/#bg_BG.UTF-8 UTF-8/bg_BG.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
echo 'FONT=UniCyr_8x16' > /etc/vconsole.conf # да ползва шрифт в tty, който поддържа кирилица

echo ""
echo "$filename"
echo "===================="
echo "| Задаване на хост |"
echo "===================="
echo "${compname}" > /etc/hostname
echo "127.0.0.1        localhost" >> /etc/hosts
echo "::1              localhost" >> /etc/hosts
printf "127.0.1.1        %s.localdomain    %s" ${compname} ${compname} >> /etc/hosts

echo ""
echo "$filename"
echo "========================="
echo "| Настройване на pacman |"
echo "========================="
sed -i 's|#Color|Color|' /etc/pacman.conf # направи го цветен
sed -i 's|#ParallelDownloads = 5|ParallelDownloads = 5|' /etc/pacman.conf # да тегли по 5 пакета успоредно
sed -i 's|SigLevel    = Required DatabaseOptional|SigLevel = Never|g' /etc/pacman.conf
sed -i 's|LocalFileSigLevel = Optional|#LocalFileSigLevel = Optional|g' /etc/pacman.conf

echo ""
echo "$filename"
echo "========================================"
echo "| Инсталиране на допълнителни програми |"
echo "========================================"
# следва масив с програми
programs=(
  # драйвери
  bluez # блутуут
  bluez-utils # блутуут

  # файлови системи
  dosfstools # за да мога да форматирам с fat файлова система
  mtools # за да мога да чета fat файлова система (gparted го изисква)
  ntfs-3g # свободна имплементация на ntfs

  # мрежи
  #inetutils # необходимо е за hostname командата, без която получавам грешка на init етапа
  connman # мениджър на връзките
  wpa_supplicant # за безжични мрежи с парола

  # инструменти за изтегляне от интернет
  wget
  xh # като curl, но на rust

  # сверяване на часовника
  ntp

  # синхронизация
  syncthing

  # сигурност
  ufw # огнена стена

  # компилиране
  base-devel # основни пакети за компилиране на програми
  linux-headers # заглавните C файлове на ядрото (също са ми нужни за компилиране на други програми)
  jdk-openjdk # java
  mono # C#
  rust # rustc и cargo
  ghc # компилатор за haskell
  #cabal-install # инструмент за изграждане на haskell пакети
  #stack # още един инструмент за изграждане на haskell пакети
  nodejs # нужен е за gitlab-ci-local например (ако не го инсалирам тук, той ще си го инсталира)
  npm # пакетен мениджър за nodejs (също за gitlab-ci-local)
  python-pip # пакетен мениджър за python
  
  # чипове
  flashrom # инструмент за флашване на чипове (и в частност BIOS чипове)
  hexyl # цветно изобразяване на hex файлове

  # контрол на версиите
  git # система за контрол на версиите
  openssh # за да мога да ползвам git (и всичко друго) с ssh
  git-delta # git diff пренаписан на rust, извеждащ много по-четлив резултат

  # подобрения за процесора
  #thermald-openrc # предпазва го от прегряване; инсталира като своя зависимост upower, което мисля, че е в конфликт с управлението на захранването от laptop-mode-tools, затова го закоментирам (no veche mozhe da go polzvam, zashtoto mahnah laptop-mode-tools)
  #cpupower-openrc # за промяна на честотата му според натоварването; не успявам да го накарам да работи
  acpid # поддръжка на ACPI (Advanced Configuration and Power Interface)
  hdparm # управление на твърди дискове (и SSD)

  # страници с ръководства
  man-db # manpages
  tealdeer # man pages чрез примери; извиква се с tldr

  # текстов редактор
  neovim

  # файлов мениджър
  xplr
  trash-cli # за изхвърляне на файловете в коша
  fzf # fuzzy finder за връщането им обратно от коша

  # обвивка
  zsh # обвивката
  zsh-completions # множество дописвания
  zsh-autosuggestions # предлага ми възможности за дописване в по-сив текст на база историята и дописванията
  zsh-syntax-highlighting # оцветяване на командите
  starship # подсказвач (превода ми на prompt)
  neofetch # извежда информация за системата
  
  # архиви
  p7zip # архиватор
  unarchiver # за .rar
  zip # за .zip

  # управление на паролите
  pass

  # звук
  #pulsemixer # wliza v konflikt s pipewire-pulse, zashtoto instalira pulseaudio

  # електронна поща
  isync # IMAP клиент, за да разполагам с писмата си и когато не съм свързан към интернет
  msmtp # SMTP клиент, за да мога да изпращам писма
  msmtp-mta # поставя символна връзка от командата sendmail към msmtp
  neomutt # за обработка на писмата
  vdirsyncer # за контактите и календарите
  khard # за обработване на контактите

  # принтер
  cups # сървър за принтиране
  avahi # автоматично откриване на други устройства в локалната мрежа вкъщи
  nss-mdns # превеждане на hostname до IP адрес чрез mDNS (Multicast DNS)
  dbus-python # зависимост за avahi-discover
  python-gobject # също зависимост за avahi-discover
  cups-pk-helper # настройване на правата за cups чрез policykit, за да не ми иска парола всеки път

  # скенер
  sane
  sane-airscan

  # пренаписани на rust
  dust # du - анализ на обема на файловете на твърдия диск
  gping # ping
  bat # cat
  bat-extras # batgrep, batman и др.
  exa # ls
  ripgrep # grep; стартира се с rg
  fd # find
  procs # ps - показва процесите
  sd # sed, но много по-простичко
  bottom # top; стартира се с btm

  # бази данни
  mariadb # базата

  # LaTeX
  texlive-bin # повечето пакети от TexLive
  texlive-langcyrillic # пакет за кирилица, който не влиза в texlive-most TODO: (beshe na parabola) ... a v texlive-bin, koeto e v arch ???
  biber # за библиографии

  # други
  bandwhich # монитор за мрежовата активност
  grex # изгражда ми регулярен израз по дадени примери
  rclone # за синхронизиране с OneDrive
)
pacman --noconfirm --needed --disable-download-timeout -S ${programs[*]}

echo ""
echo "$filename"
echo "=========================="
echo "| Сверяване на часовника |"
echo "=========================="
ntpd -qg # изтегли времето от NTP сървър; -q му казва да се затвори след това; -g позволява разлика > 15 минути
ln -sf /usr/share/zoneinfo/Europe/Sofia /etc/localtime
hwclock --systohc

echo ""
echo "$filename"
echo "==============="
echo "| Потребители |"
echo "==============="
useradd -m -G wheel,uucp -s /bin/zsh ${username}
printf "$password\n$password" | passwd ${username}
sed -i 's/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/g' /etc/sudoers

echo ""
echo "$filename"
echo "======================="
echo "| Инсталиране на paru |"
echo "======================="
mkdir -p /home/$name/.local/src
git clone --depth 1 "https://aur.archlinux.org/paru-bin" "/home/$name/.local/src/paru-bin"
chown -R ${username}:wheel /home/$name/.local/
cd "/home/$name/.local/src/paru-bin"
sudo -u ${username} makepkg --noconfirm -si

echo ""
echo "$filename"
echo "===================================="
echo "| Инсталиране на програми (с paru) |"
echo "===================================="
# следва масив с програми
programs=(
  nvim-packer-git # пакетния мениджър за nvim под lua
  neovim-lualine-git # за лентата на състоянието в nvim
  #cpupower-gui # графична обвивка за управление на cpupower (инсталиран по-горе), който не успявам да накарам да работи
  gitlab-ci-local # за локално тестване на gitlab pipeline-и
  tllocalmgr # пакетен мениджър за LaTeX
)
sudo -u ${username} paru --noconfirm --needed --disable-download-timeout -S ${programs[*]}

echo ""
echo "$filename"
echo "==============================="
echo "| Инсталиране на LaTeX пакети |"
echo "==============================="
tllocalmgr install enumitem
tllocalmgr install subfiles
tllocalmgr install import
tllocalmgr install xstring
sudo texhash

#echo ""
#echo "$filename"
#echo "=========================="
#echo "| Настрой огнената стена |"
#echo "=========================="
# TODO: да разбера защо не работи, както и как да настроя connman да се свърже към wifi мрежата още от тук, а не чак след инсталацията
#ufw enable # изпълни скрипт (вграден в ufw, не писан от мен) за задействането ѝ
#ufw default deny # забрани входящия трафик
#ufw allow qbittorrent # отвори портовете на qbittorent

echo ""
echo "$filename"
echo "===================="
echo "| Настрой демоните |"
echo "===================="
systemctl enable ntpd.service
systemctl enable ufw.service
systemctl enable connman.service
systemctl enable bluetooth.service
systemctl enable acpid.service # поддръжка на ACPI (Advanced Configuration and Power Interface)
#rc-update add hdparm default # ddуправление на твърди дискове (и SSD)
#rc-update add laptop_mode default # обединяваща програма за всички настройки за лаптоп
systemctl enable syncthing@"${username}".service # синхронизация на файлове между всички устройства в локалната ми мрежа вкъщи
#rc-update add powertop default # добавен от мен файл в /etc/init.d, който стартира автоматична powertop настройка (трябват му администраторски привилегии); ползвам го за да спра досадното писукане при изваждане на захранващия кабел и при неуспешно търсене в браузър и други такива случаи
systemctl enable cups.socket # сървър за принтиране
systemctl enable avahi-daemon.service # автоматично откриване на принтери в локалната мрежа
systemctl enable saned.socket # сървър за скенера

echo ""
echo "$filename"
echo "============================="
echo "| Премахване на GRUB менюто |"
echo "============================="
sed -i 's|GRUB_TIMEOUT_STYLE=console|GRUB_TIMEOUT_STYLE=hidden|' /etc/default/grub # да не се показва
sed -i 's|GRUB_TIMEOUT=5|GRUB_TIMEOUT=0|' /etc/default/grub # да не чака (иначе няма да се показва, но ще си чака така)
grub-install --target=i386-pc ${diskname}
grub-mkconfig -o /boot/grub/grub.cfg

echo ""
echo "$filename"
echo "=================================="
echo "| Изтриване на излишните файлове |"
echo "=================================="
rm /home/"${username}"/.bash_logout /home/"${username}"/.bash_profile /home/"${username}"/.bashrc

echo ""
echo "$filename"
echo "========================================="
echo "| Изтегляне на тези скриптове от gitlab |"
echo "========================================="
cd /home/${username}
git clone https://gitlab.com/gnx/gnu-linux-installs

echo ""
echo "$filename"
echo "================================"
echo "| Създаване на символни връзки |"
echo "================================"
USER_HOME="/home/${username}"
DOTFILES="$USER_HOME/gnu-linux-installs/dotfiles"
CONFIG="$USER_HOME/.config"
LOCAL="$USER_HOME/.local"
CACHE="$USER_HOME/.cache"
mkdir -p $CONFIG/zsh
mkdir -p $LOCAL/bin
#ln -sf $DOTFILES/laptop-mode/conf.d/auto-hibernate.conf /etc/laptop-mode/conf.d/auto-hibernate.conf
ln -sf $DOTFILES/polkit/49-nopasswd_global.rules /etc/polkit-1/rules.d/49-nopasswd_global.rules
ln -sf $DOTFILES/polkit/49-allow-passwordless-printer-admin.rules /etc/polkit-1/rules.d/49-allow-passwordless-printer-admin.rules
ln -sf $DOTFILES/initramfs/mkinitcpio.conf /etc/mkinitcpio.conf
ln -sf $DOTFILES/avahi/nsswitch.conf /etc/nsswitch.conf
ln -sf $DOTFILES/udev/rules.d /etc/udev/rules.d
ln -sf $DOTFILES/zsh/parabola/.zshenv $USER_HOME/.zshenv
ln -sf $DOTFILES/шаблони $USER_HOME/шаблони
ln -sf $DOTFILES/zsh/parabola/.zshrc $CONFIG/zsh/.zshrc
ln -sf $DOTFILES/zsh/parabola/.zlogin $CONFIG/zsh/.zlogin
ln -sf $DOTFILES/git $CONFIG/git
ln -sf $DOTFILES/nvim $CONFIG/nvim
ln -sf $DOTFILES/xplr $CONFIG/xplr
ln -sf $DOTFILES/mutt $CONFIG/mutt
ln -sf $DOTFILES/msmtp $CONFIG/msmtp
ln -sf $DOTFILES/sxhkd $CONFIG/sxhkd
ln -sf $DOTFILES/khard $CONFIG/khard
ln -sf $DOTFILES/notmuch $CONFIG/notmuch
ln -sf $DOTFILES/neofetch $CONFIG/neofetch
ln -sf $DOTFILES/hristo-bin $LOCAL/bin/hristo

echo ""
echo "$filename"
echo "======================="
echo "| Настройване на mutt |"
echo "======================="
mkdir -p $CACHE/mutt

echo ""
echo "$filename"
echo "======================="
echo "| Настройване на mutt |"
echo "======================="
mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "=========================="
echo "| Свързване към интернет |"
echo "=========================="
iwctl station wlan0 scan
iwctl station wlan0 connect gnx


#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "============================"
echo "| Задаване на променливите |"
echo "============================"
read -p "Disk: " diskname # -p извежда надписа след себе си като подсказка и не преминава на нов ред
read -p "Компютър: " compname
read -p "Потребител: " username
read -s -p "Парола: " password # -s е за да не се извежда паролата на екрана докато я пишем

echo ""
echo "$filename"
echo "====================================="
echo "| Извикване на останалите скриптове |"
echo "====================================="
./2-install-base.sh ${diskname} ${compname} ${username} ${password}
./3-0-tty-setup.sh ${diskname} ${compname} ${username} ${password}
./4-0-gui-setup.sh ${diskname} ${compname} ${username} ${password}
./5-finish.sh ${diskname} ${compname} ${username} ${password}

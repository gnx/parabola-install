#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "==============="
echo "| Локализация |"
echo "==============="
setfont UniCyr_8x16 # да ползва шрифт в tty, който поддържа кирилица

echo ""
echo "$filename"
echo "=========================="
echo "| Сверяване на часовника |"
echo "=========================="
ln -sf /usr/share/zoneinfo/Europe/Sofia /etc/localtime

echo ""
echo "$filename"
echo "========================="
echo "| Настройване на pacman |"
echo "========================="
sed -i 's|#Color|Color|' /etc/pacman.conf # направи го цветен
sed -i 's|#ParallelDownloads = 5|ParallelDownloads = 5|' /etc/pacman.conf # да тегли по 5 пакета успоредно
sed -i 's|SigLevel    = Required DatabaseOptional|SigLevel = Never|g' /etc/pacman.conf # да не проверява ключодържатели
sed -i 's|LocalFileSigLevel = Optional|#LocalFileSigLevel = Optional|g' /etc/pacman.conf # също за ключодържателите

#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "=============================================================================="
echo "| Задай на скрипта да се прекрати, ако някоя команда върне код различен от 0 |"
echo "=============================================================================="
set -e

echo ""
echo "$filename"
echo "============================"
echo "| Задаване на променливите |"
echo "============================"
wifiname=
wifipass=
diskname=
compname=
username=
password=

echo ""
echo "$filename"
echo "==============================================="
echo "| Инсталиране на графични програми (с pacman) |"
echo "==============================================="
# следва масив с програми
programs=(

  # драйвери
  xf86-video-intel # видео драйвер
  xf86-input-synaptics # тъчпад (неподдържан и непрепоръчван драйвер, който обаче позволява жестове с по 2-3 пръста)

  # графичен сървър
  xorg-server # графичният сървър
  xorg-xinit # програмата за стартирането му
  xorg-setxkbmap # смяна на клавиатурните подредби
  xclip # мениджър за копирането, za da moga da kopiram ot vim
  xbindkeys # задействане на команди чрез клавишни комбинации (за да мога да управлявам силата на звука от клавиатурата)
  xdotool # за да се изобразяват изображения в st

  # десктоп
  openbox # мениджър за прозорците
  menumaker # създаване на меню за openbox с всички инсталирани програми
  picom # композитор
  polybar # лента
  hsetroot # за задаване на тапет със замъгляване
  sxhkd # за задаване на клавишни комбинации
  dunst # уведомления на работния плот
  rofi # бързо стартиране на програми

  # мениджър на сесиите
  sddm
  qt5-graphicaleffects # необходим е за темата ми (която е производна на sugar-candy)
  qt5-quickcontrols2 # също за темата
  lxqt-policykit # за polkit агента

  # управление на дискове
  gparted

  # сигурност
  gufw # графичен клиент за огнената стена (инсталирана в предишния етап)

  # управление на паролите
  qtpass # графичната обвивка за pass
  passff-host # посредникът между файловата система и passff в браузъра

  # архиви
  xarchiver # архиватор с GTK3

  # настройване на екраните
  arandr # инструмент за подредба на мониторите

  # терминали
  xterm # терминал - основен
  # alacritty # терминал - готин
  chafa # за изрисуване на изображения в терминала (и в частност в neofetch)

  # браузъри
  firefox-i18n-bg # браузър
  # iceweasel-noscript # спира JS

  # среди за разработка
  arduino # ардуино
  arduino-avr-core # библиотеката с платки и инструменти за работа с тях

  # платки
  kicad
  kicad-library
  kicad-library-3d

  # торент
  qbittorrent

  # файлови мениджъри
  pcmanfm-qt # qt вариант на pcmanfm
  gvfs # за да достъпвам кошчето през pcmanfm и да се зареждат автоматично флашките
  gvfs-mtp # за да разпознава телефона ми
  handlr # определя с кои програми да се отварят по подразбиране файловете с определен тип (заместител на xdg_open)

  # текстови редактори
  mousepad # общ текстов редактор

  # шрифтове
  ttf-dejavu # общи шрифтове
  noto-fonts-emoji # емоджита
  ttf-font-awesome # иконки
  ttf-liberation # ползвам го в LaTeX
  # ttf-iosevka-nerd # nerd шрифт - вероятно може да замести останалите три шрифта по-горе; TODO: да проверя дали наистина е така

  # звук
  pipewire # звуков сървър
  pipewire-pulse # съвместимост с PulseAudio програми
  pipewire-jack # съвместимост с Jack програми
  rtkit # дава права на pipewire за realtime (като jack); без него не тръгва
  #pavucontrol-qt # за управление на звука
  audacity # обработка на аудио
  rosegarden # писане на midi музика
  qsynth # аудио синтезатор
  freepats-general-midi # звуков шрифт
  qpwgraph # навързване на кабелите

  # видео
  mpv # видео плейър
  yt-dlp # теглене на видеа от youtube
  ffmpeg # обработка на видео от конзолата

  # изображения
  imagemagick # обработване на изображения през конзолата
  gimp # обработка на растерни изображения
  krita # професионално рисуване
  mypaint # простичко рисуване
  mypaint-brushes # допълнителни четки
  inkscape # векторна графика

  # виртуални машини
  dnsmasq # за default NAT/DHCP на гост системата TODO: да проверя дали наистина ми трябва
  dmidecode # също за интернета в гост системата
  qemu-full # QEMU
  libvirt # общ интерфейс за управление на виртуални машини с всякакви хипервайзъри - KVM/QEMU, Xen, LXC, VirtualBox и други
  nftables # необходим за libvirtd демона (service)
  firewalld # необходим за libvirtd демона (service)
  virt-manager # графична програма за libvirt

  # офис
  libreoffice-fresh-bg
  
  # pdf
  zathura-pdf-poppler

  # принтер
  system-config-printer # графична програма за настройване на принтерите

  # 2D
  librecad
  qcad

  # 3D
  blender
  freecad

  # разкрасяване
  kvantum # за QT темите
  qt5ct # за добавяне на шрифтове (българска кирилица) към темите от kvantum

)
pacman --noconfirm --needed --disable-download-timeout -S ${programs[*]}

echo ""
echo "$filename"
echo "================================================================================="
echo "| Инсталиране на графични програми (с paru) |"
echo "================================================================================="
mkdir -p /home/${username}/.config/wal/colorschemes/light
mkdir -p /home/${username}/.config/wal/colorschemes/dark
mkdir -p /home/${username}/.config/wal/templates
# следва масив с програми
programs=(
  mint-y-icons # икони
  mint-themes-git # gtk тема
  xbanish # скриване на стрелката на мишката, когато натисна клавиш от клавиатурата
  leftwm # мениджър за прозорците
  buku # отметки за браузъра
  bukubrow-bin # програма за връзка между buku и браузъра
  pywal-16-colors # задаване на тапет и тема за терминала
  python-pywalfox # pywal тема за firefox (и iceweasel и thunderbird)
  signal-desktop-beta-bin # за чат и обаждания, но сървърът не помни нищо, само свързва двата края и се оттегля
  element-desktop-nightly-bin # също за чат, но със стаи и сървърът помни историята (но може да е криптирана)
  eclipse-java # среда за разработка на java
  marp-cli-bin # презентации
  oauth2token # получаване на token чрез oauth - трябва ми за gmail локалното, офлайн копие
  cyrus-sasl-xoauth2-git # свързва oauth2token с isync
  goimapnotify # слушател за PUSH събития от IMAP сървър
  #xdg-utils-handlr # замества xdg_open с handlr; ne moga da go instaliram avtomatichno, zashtoto e v konfikt s xdg-utils i paru ne mi dava da go instaliram pri --noconfirm
  veyon # управление на други компютри в локалната мрежа
  digital # симулатор за цифрова логика
  # qpwgraph-qt5 # свързване на аудио и видео кабелите; компилира се около 5 минути
  # qbittorrent-qt5 # компилира се около 30 минути
)
sudo -u ${username} paru --noconfirm --needed --disable-download-timeout -S ${programs[*]}

echo ""
echo "$filename"
echo "========================"
echo "| Настройване на pywal |"
echo "========================"
mkdir -p /home/${username}/.local/share/wallpapers # TODO: bez tova wallpaper-a izchezva sled kato se zamagli. Veroqtno moga da go oprostq, ako promenq nastroikite na programata, koqto mi zadava wallpaper-a sled pywal
pywalfox install # задай темата от pywal на firefox

echo ""
echo "$filename"
echo "======================="
echo "| Настройване на buku |"
echo "======================="
printf 'n\nn\nn\nn\nn\nn' | sudo -u ${username} buku --nostdin --ai # стартирай buku за пръв път, за да си създаде база данни, иначе следващата стъпка ще върне грешка
sudo -u ${username} bukubrow --install-firefox

echo ""
echo "$filename"
echo "===================="
echo "| Настрой демоните |"
echo "===================="
systemctl enable sddm.service
systemctl enable nftables.service
systemctl enable firewalld.service
systemctl enable libvirtd.service

echo ""
echo "$filename"
echo "================"
echo "| Настрой mutt |"
echo "================"
mkdir -p /home/${username}/.mail/google
mkdir -p /home/${username}/.mail/disroot
mkdir -p /home/${username}/.mail/posteo
mkdir -p /home/${username}/.local/share/oauth2token/google

echo ""
echo "$filename"
echo "================================"
echo "| Създаване на символни връзки |"
echo "================================"
DOTFILES="/home/$username/gnu-linux-installs/dotfiles"
USER_HOME="/home/$username"
CONFIG="$USER_HOME/.config"
ln -sf $DOTFILES/firefox/extensions /usr/lib/firefox/browser/extensions
ln -sf $DOTFILES/icons/cursors-theme-color-ring-light /usr/share/icons/cursors-theme-color-ring-light
ln -sf $DOTFILES/applications/org.pwmt.zathura.desktop.in /usr/share/applications/org.pwmt.zathura.desktop.in
ln -sf $DOTFILES/sddm/sddm.conf /etc/sddm.conf
ln -sf $DOTFILES/xorg /etc/X11/xorg.conf.d
ln -sf $DOTFILES/xbindkeys/.xbindkeysrc $USER_HOME/.xbindkeysrc
ln -sf $DOTFILES/gtk/.gtkrc-2.0 $USER_HOME/.gtkrc-2.0
ln -sf $DOTFILES/themes $USER_HOME/.local/share/themes
ln -sf $DOTFILES/isync/.mbsyncrc $USER_HOME/.mbsyncrc
ln -sf $DOTFILES/fonts $USER_HOME/.fonts
ln -sf $DOTFILES/gtk/gtk-3.0 $CONFIG/gtk-3.0
ln -sf $DOTFILES/rofi $CONFIG/rofi
ln -sf $DOTFILES/qt5ct $CONFIG/qt5ct
ln -sf $DOTFILES/picom $CONFIG/picom
ln -sf $DOTFILES/handlr $CONFIG/handlr
ln -sf $DOTFILES/Kvantum $CONFIG/Kvantum
ln -sf $DOTFILES/openbox $CONFIG/openbox
ln -sf $DOTFILES/polybar $CONFIG/polybar
ln -sf $DOTFILES/zathura $CONFIG/zathura
ln -sf $DOTFILES/autostart $CONFIG/autostart
ln -sf $DOTFILES/pcmanfm-qt $CONFIG/pcmanfm-qt
ln -sf $DOTFILES/oauth2token $CONFIG/oauth2token
ln -sf $DOTFILES/imapnotify $CONFIG/imapnotify
ln -sf $DOTFILES/mimeapps/mimeapps.list $CONFIG/mimeapps.list
ln -sf $DOTFILES/starship/starship.toml $CONFIG/starship.toml
ln -sf $DOTFILES/xdg-user-dirs/user-dirs.dirs $CONFIG/user-dirs.dirs
ln -sf $DOTFILES/xdg-user-dirs/user-dirs.locale $CONFIG/user-dirs.locale
cp -r $DOTFILES/leftwm $CONFIG/leftwm # копирам го, вместо да направя symlink, защото скриптът wallpaper-chooser пише върху файлът с настройки на темата на leftwm, което означава, че ако ползвам symlink, при всяка промяна на темата от pywal ще имам разлики в това git хранилище. gitignore на този файл също не е добър вариант, защото понякога искам да го променя и да го кача в gitlab
cp -r $DOTFILES/sddm/themes/hristo /usr/share/sddm/themes/hristo
cp $DOTFILES/xsessions/leftwm.desktop /usr/share/xsessions/leftwm.desktop
cp $DOTFILES/xsessions/openbox.desktop /usr/share/xsessions/openbox.desktop

echo ""
echo "$filename"
echo "================================"
echo "| teglene na fonovete|"
echo "================================"
/home/${username}/.local/bin/hristo/wallpaper-downloader.sh

echo ""
echo "$filename"
echo "================================"
echo "| st terminal |"
echo "================================"
git clone https://gitlab.com/gnx/st
cd st
make install

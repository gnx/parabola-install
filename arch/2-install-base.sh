#!/usr/bin/env bash

filename=$(basename $0) # вземи името на този файл

echo ""
echo "$filename"
echo "=============================================================================="
echo "| Задай на скрипта да се прекрати, ако някоя команда върне код различен от 0 |"
echo "=============================================================================="
set -e

echo ""
echo "$filename"
echo "============================"
echo "| Задаване на променливите |"
echo "============================"
if (( $# == 0 )) # ако този скрипт е извикан без да са му подадени променливи (значи го изпълнявам самостоятелно)
then
  read -p "Disk: " diskname # -p извежда надписа след себе си като подсказка и не преминава на нов ред
  read -p "Компютър: " compname
  read -p "Потребител: " username
  read -s -p "Парола: " password # -s е за да не се извежда паролата на екрана докато я пишем
else # ако е извикан с променливи (значи го вика главния скрипт и вече съм ги задал в него)
  diskname="$1"
  compname="$2"
  username="$3"
  password="$4"
fi

echo ""
echo "$filename"
echo "============================"
echo "| Заделяне на твърдия диск |"
echo "============================"
parted -s $diskname mklabel gpt # създай нова gpt таблица
sgdisk -n 0:0:+1M $diskname # 1 - специален дял при bios + grub + gpt комбинация
sgdisk --typecode=1:ef02 $diskname # сложи флаг за boot на специалния дял
sgdisk -n 0:0:+9G $diskname # 2 - swap дял
sgdisk -n 0:0:+50G $diskname # 3 - root дял
sgdisk -n 0:0:0 $diskname # 4 - home дял

echo ""
echo "$filename"
echo "==============="
echo "| Форматиране |"
echo "==============="
mkswap -L 'swap' ${diskname}2
yes | mkfs.ext4 -L'root' ${diskname}3
yes | mkfs.ext4 -L'home' ${diskname}4

echo ""
echo "$filename"
echo "========================="
echo "| Зареждане на дяловете |"
echo "========================="
swapon /dev/disk/by-label/swap
mount /dev/disk/by-label/root /mnt
mkdir /mnt/home
mount /dev/disk/by-label/home /mnt/home

echo ""
echo "$filename"
echo "==============================="
echo "| Инсталирай базовата система |"
echo "==============================="
pacstrap -K /mnt base linux linux-firmware grub # -K syzdava prazen keyring v /mnt

echo ""
echo "$filename"
echo "============================================="
echo "| Задай автоматичното зареждане на дяловете |"
echo "============================================="
genfstab -U /mnt >> /mnt/etc/fstab

echo ""
echo "$filename"
echo "======================="
echo "| Настройване на GRUB |"
echo "======================="
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt grub-install --target=i386-pc ${diskname}
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

echo ""
echo "$filename"
echo "================================="
echo "| Настройване на парола за root |"
echo "================================="
printf "$password\n$password" | arch-chroot /mnt passwd
